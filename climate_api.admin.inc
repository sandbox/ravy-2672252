<?php
/**
 * @file
 * Administrative pages for the VeevaVault.
 */

/**
 * Generate the VeevaVault authorization form.
 *
 * @param array $form_state
 *   Regular drupal form state values.
 *
 * @return array
 *   The VeevaVault authorization form.
 */
function climate_api_auth_code_form($form, &$form_state) {

  $form = array();
  $climate_api_client_id = variable_get('climate_api_client_id');
  $climate_api_secret = variable_get('climate_api_secret');
  $climate_api_endpoint = variable_get('climate_api_endpoint');
  $redirect_uri = variable_get('redirect_uri');



  $form['message'] = array(
    '#type' => 'item',
    '#markup' => t('Authorize this website to communicate with Climate Api by entering the client ID and Client Secret from a remote application. Clicking authorize will check for established session.'),
  );

  // Advance fieldset
  $form['advanced'] = array(
    '#title' => t('Advanced'),
    '#type' => 'fieldset',
  );

  $form['advanced']['climate_api_endpoint'] = array(
    '#title' => t('Climate Api Endpoint'),
    '#type' => 'textfield',
    '#description' => t('Enter the URL of your Climate Api environment (for example, <code>https://climate.com/static/app-login/index.html).'),
    '#default_value' => $climate_api_endpoint,
    '#required' => TRUE,
  );

  // Atrributes:
  // Page.
  $form['page'] = array(
    '#title' => t('Climate API Page Attribute'),
    '#type' => 'textfield',
    '#description' => t('page endpoint'),
    '#required' => TRUE,
    '#default_value' => 'oidcauthn',
  );

  // Mobile.
  $form['mobile'] = array(
    '#title' => t('Climate API Mobile Attribute'),
    '#type' => 'textfield',
    '#description' => t('mobile true/false. default is true'),
    '#required' => TRUE,
    '#default_value' => 'true',
  );

  // Response Type.
  $form['response_type'] = array(
    '#title' => t('Climate API Response Type Attribute'),
    '#type' => 'textfield',
    '#description' => t('set the response type needed. for GET auth req use code'),
    '#required' => TRUE,
    '#default_value' => 'code'
  );

  // Scope Type.
  $form['scope'] = array(
    '#title' => t('Climate API request scope'),
    '#type' => 'textfield',
    '#description' => t('Scope of request'),
    '#required' => TRUE,
    '#default_value' => 'openid+user',
  );

  $form['submit'] = array(
    '#value' => t('Establish Connection'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Submit handler for climate_api_auth_form().
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function climate_api_auth_code_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  variable_set('climate_api_client_id', $values['climate_api_client_id']);
  variable_set('climate_api_secret', $values['climate_api_secret']);
  variable_set('climate_api_endpoint', $values['climate_api_endpoint']);
  variable_set('redirect_uri', $values['redirect_uri']);

  // @TODO not needed in variable set maybe remov in future and make static.
  // variable_set('page', $values['page']);
  // variable_set('mobile', $values['mobile']);
  // variable_set('response_type', $values['response_type']);
  // variable_set('scope', $values['scope']);

  $params = array(
    'scope' => $values['scope'],
    'page' => $values['page'],
    'mobile' => $values['mobile'],
    'response_type' => $values['response_type'],
    'client_id' => $values['climate_api_client_id'],
    'redirect_uri' => variable_get('redirect_uri'),
    );


  // Call code for getting client code.
  $climate_api_client_code = climate_api_get_client_code($params);
  if (isset($climate_api_client_code)) {
    drupal_set_message(t('Climate Api connection code establish : SUCCESS<br/>Code: @code', array('@code' => $climate_api_client_code)), 'status');
  }
  else {
    drupal_set_message(t('Unauthorized Session'), 'error');
  }
}


function climate_api_auth_token_callback() {
  $params = drupal_get_query_parameters();
  // User credentials.
  $client_id = variable_get('climate_api_client_id');
  $secret = variable_get('climate_api_secret');
  $redirect_uri = variable_get('redirect_uri');

  $access = $client_id . ':' . $secret;
  $auth_encoded = base64_encode($access);
  $params['grant_type'] = 'authorization_code';
  $params['scope'] = 'openid user';
  $params['redirect_uri'] = variable_get('redirect_uri');
 // Get new session id.
  $url = 'https://climate.com/api/oauth/token' . '?' . http_build_query($params);

  // Build Options.
  $options = array(
    'headers' => array(
      'Content-Type' => 'application/x-www-form-urlencoded',
      'Authorization' => 'Basic '. $auth_encoded,
    ),
    // 'data' => http_build_query($params),
    'method' => 'POST',
  );

  $response = drupal_http_request($url, $options);
  $out_obj = drupal_json_decode($response->data);
  $id_token = $out_obj['id_token'];
  variable_set('id_token', $id_token);
  drupal_set_message(t('Token for this account created! @code', array('@code' => $id_token)), 'status');
  // drupal_set_message(t('Complete user object received', 'status'));
  drupal_render(dsm($out_obj));

  return 'Bingo!'; //theme('Token can be used to create any query to climate api.');

}


function climate_api_query_form($form, &$form_state) {

  $form = array();
  $climate_api_client_id = variable_get('climate_api_client_id');
  $climate_api_secret = variable_get('climate_api_secret');
  $climate_api_endpoint = variable_get('climate_api_endpoint');
  $redirect_uri = variable_get('redirect_uri');
  $id_token = variable_get('id_token');




  $form['message'] = array(
    '#type' => 'item',
    '#markup' => t('Authorize this website to communicate with Climate Api by entering the client ID and Client Secret from a remote application. Clicking authorize will check for established session.'),
  );
  // Client ID.
  $form['climate_api_client_id'] = array(
    '#title' => t('Climate Api Client ID'),
    '#type' => 'textfield',
    '#description' => t('Client API ID'),
    '#required' => TRUE,
    '#default_value' => $climate_api_client_id,
  );

    // Redirect URL.
  $form['redirect_uri'] = array(
    '#title' => t('Climate API Post Authorization Redirect URL Attribute'),
    '#type' => 'textfield',
    '#description' => t('Post redirection redirect URL'),
    '#required' => TRUE,
    '#default_value' => $redirect_uri,
  );

  // Client Secret 
  $form['climate_api_secret'] = array(
    '#title' => t('Climate Api Client Secret'),
    '#type' => 'password',
    '#description' => t('Client secret'),
    '#required' => TRUE,
    '#default_value' => $climate_api_secret,
  );

  // Advance fieldset
  $form['advanced'] = array(
    '#title' => t('Advanced'),
    '#type' => 'fieldset',
  );

  $form['advanced']['climate_api_endpoint'] = array(
    '#title' => t('Climate Api Endpoint'),
    '#type' => 'textfield',
    '#description' => t('Enter the URL of your Climate Api environment (for example, <code>https://climate.com/static/app-login/index.html).'),
    '#default_value' => $climate_api_endpoint,
    '#required' => TRUE,
  );

  // Atrributes:
  // Page.
  $form['page'] = array(
    '#title' => t('Climate API Page Attribute'),
    '#type' => 'textfield',
    '#description' => t('page endpoint'),
    '#required' => TRUE,
    '#default_value' => 'oidcauthn',
  );

  // Mobile.
  $form['mobile'] = array(
    '#title' => t('Climate API Mobile Attribute'),
    '#type' => 'textfield',
    '#description' => t('mobile true/false. default is true'),
    '#required' => TRUE,
    '#default_value' => 'true',
  );

  // Response Type.
  $form['response_type'] = array(
    '#title' => t('Climate API Response Type Attribute'),
    '#type' => 'textfield',
    '#description' => t('set the response type needed. for GET auth req use code'),
    '#required' => TRUE,
    '#default_value' => 'code'
  );

  // Scope Type.
  $form['scope'] = array(
    '#title' => t('Climate API request scope'),
    '#type' => 'textfield',
    '#description' => t('Scope of request'),
    '#required' => TRUE,
    '#default_value' => 'openid+user',
  );

  $form['submit'] = array(
    '#value' => t('Establish Connection'),
    '#type' => 'submit',
  );

  return $form;
}

/**
 * Submit handler for climate_api_auth_form().
 *
 * @param array $form
 *   The form array.
 * @param array $form_state
 *   The form state array.
 */
function climate_api_query_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $params = array(
    'scope' => $values['scope'],
    'page' => $values['page'],
    'mobile' => $values['mobile'],
    'response_type' => $values['response_type'],
    'client_id' => $values['climate_api_client_id'],
    'redirect_uri' => variable_get('redirect_uri'),
    );


  // Call code for getting client code.
  $climate_api_run_query = climate_api_run_query($params);
  if (isset($climate_api_run_query)) {
    drupal_set_message(t('Climate Api Query: SUCCESS<br/>Code: @code', array('@code' => $climate_api_client_code)), 'status');
  }
  else {
    drupal_set_message(t('Unauthorized Session'), 'error');
  }
}
